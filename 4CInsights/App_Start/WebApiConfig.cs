﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Newtonsoft.Json.Serialization;
using _4CInsights.Handlers;

namespace _4CInsights
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // By default, we are initializing the WebApi HttpConfiguration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Default.
            // This means that during exception handling, the setting for <system.web><customErrors mode=””/></system.web>
            // will be followed to determine if exception details are returned to the caller or just some generic message.
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Default;

            config.Services.Replace(typeof(IExceptionHandler), new ApiExceptionHandler());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
