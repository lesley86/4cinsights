﻿
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApi.StructureMap;
using _4cInsights.DependencyResolution.Registries.DataAccess;
using _4cInsights.DependencyResolution.Registries.Service;

namespace _4CInsights
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.UseStructureMap(x =>
            {
                x.AddRegistry<ServiceRegistry>();
                x.AddRegistry<DataAccessRegistry>();
            });
        }
    }
}
