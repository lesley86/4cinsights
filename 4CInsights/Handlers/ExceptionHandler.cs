﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using _4cInsights.Core.Exceptions;

namespace _4CInsights.Handlers
{
    public class ApiExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            if (context != null)
            {
                // An exception handler indicates that it has handled an exception by setting the
                // Result property to an action result (for example, an ExceptionResult,
                // InternalServerErrorResult, StatusCodeResult, or a custom result). 
                // If the Result property is null, the exception is unhandled and the original
                // exception will be re-thrown.
                context.Result = HandleExceptionResult(context.ExceptionContext);
            }
        }

        private IHttpActionResult HandleExceptionResult(ExceptionContext context)
        {
            Exception exception = context.Exception;

            HttpRequestMessage request = context.Request;
            if (request == null)
            {
                return null;
            }

            HttpRequestContext requestContext = context.RequestContext;
            if (requestContext == null)
            {
                return null;
            }

            HttpConfiguration configuration = requestContext.Configuration;
            if (configuration == null)
            {
                return null;
            }

            IContentNegotiator contentNegotiator = configuration.Services.GetContentNegotiator();
            if (contentNegotiator == null)
            {
                return null;
            }

            return GetExceptionActionResult(exception, requestContext.IncludeErrorDetail, contentNegotiator, request,
                configuration.Formatters);
        }

        private IHttpActionResult GetExceptionActionResult(
            Exception exception,
            bool includeErrorDetail,
            IContentNegotiator contentNegotiator,
            HttpRequestMessage request,
            IEnumerable<MediaTypeFormatter> formatters)
        {
            
            if (exception is NotFoundException)
                return new NotFoundResult(request);

            if (exception is _4cInsights.Core.Exceptions.ValidationException)
                return new BadRequestResult(request);

            return new InternalServerErrorResult(request);
        }
    }
}