﻿namespace _4CInsights.Models
{
    public class DiffItemApiResponse
    {
        public int Offset { get; set; }
        public int Length { get; set; }
    }
}