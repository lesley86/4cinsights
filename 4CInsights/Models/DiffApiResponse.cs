﻿using System.Collections.Generic;

namespace _4CInsights.Models
{
    public class DiffApiResponse
    {
        public string DffResultType { get; set; }

        public IEnumerable<DiffItemApiResponse> Diffs { get; set; }
    }

}