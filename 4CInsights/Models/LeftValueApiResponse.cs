﻿using System;

namespace _4CInsights.Models
{
    public class LeftValueApiResponse
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }

}