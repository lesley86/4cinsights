﻿namespace _4CInsights.Models
{
    public class RightValueApiResponse
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }

}