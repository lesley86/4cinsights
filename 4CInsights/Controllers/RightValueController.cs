﻿using _4cInsights.Core.Service.Models.External;
using _4cInsights.Core.Services.Interface;
using _4CInsights.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace _4CInsights.Controllers
{
    /// <summary>
    /// The endpoints for creating and fetching the right valuess. 
    /// </summary>
    [RoutePrefix("v1/diff/{id:int}/right")]
    public class RightValueController : ApiController
    {
        private readonly IRightValueService RightValueService;

        public RightValueController(IRightValueService RightValue)
        {
            this.RightValueService = RightValue;
        }

        /// <summary>
        /// Fetch the right value
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("")]
        [ResponseType(typeof(HttpResponseMessage))]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var result = RightValueService.GetRightValue(id);
            var returnResult = new RightValueApiResponse
            {
                Id = result.Id,
                Data = result.Data
            };

            return Request.CreateResponse(HttpStatusCode.OK, returnResult);
        }

        /// <summary>
        /// Create or update the right data value for a given identifier
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("")]
        [ResponseType(typeof(IHttpActionResult))]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]RightValuePutRequest value)
        {
            var leftRequest = new UpdateRightValueServiceRequest
            {
                Id = id,
                Data = value.Data
            };

            var result = RightValueService.CreateOrUpdateRightValue(leftRequest);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
