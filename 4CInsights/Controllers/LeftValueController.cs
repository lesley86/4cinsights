﻿using System.Web.Http;
using _4CInsights.Models;
using _4cInsights.Core.Services.Interface;
using System.Web.Http.Description;
using System.Net;
using System.Net.Http;
using _4cInsights.Core.Service.Models.External;

namespace _4CInsights.Controllers
{
    /// <summary>
    /// The endpoints for creating and fetching the left valuess. 
    /// </summary>
    [RoutePrefix("v1/diff/{id:int}/left")]
    public class LeftValueController : ApiController
    {
        private readonly ILeftValueService leftValueService;

        public LeftValueController(ILeftValueService leftValue)
        {
            this.leftValueService = leftValue;
        }

        /// <summary>
        /// Fetch the left value response
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("")]
        [ResponseType(typeof(HttpResponseMessage))]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var result = leftValueService.GetLeftValue(id);
            var returnResult = new LeftValueApiResponse
            {
                Id = result.Id,
                Data = result.Data
            };

            return Request.CreateResponse(HttpStatusCode.OK, returnResult);
        }

        /// <summary>
        /// Create or update the left data value for a given identifier
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("")]
        [ResponseType(typeof(IHttpActionResult))]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody]LeftValuePutRequest value)
        {
            var leftRequest = new UpdateLeftValueServiceRequest
            {
                Id = id,
                Data = value.Data
            };

            var result = leftValueService.CreateOrUpdateLeftValue(leftRequest);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
