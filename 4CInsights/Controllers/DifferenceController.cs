﻿using System.Linq;
using _4cInsights.Core.Services.Interface;
using _4CInsights.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace _4CInsights.Controllers
{
    /// <summary>
    /// The controller to return the differences for the left and right values.
    /// </summary>
    [RoutePrefix("v1/diff/{id:int}")]
    public class DifferenceController : ApiController
    {
        private readonly IDiffService diffService;

        public DifferenceController(IDiffService diffService)
        {
            this.diffService = diffService;
        }


        [Route("")]
        [ResponseType(typeof(HttpResponseMessage))]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var result = diffService.GetDifferences(id);
            var returnResult = new DiffApiResponse
            {
                DffResultType = result.DffResultType,
                Diffs = result.Diffs?.Select(x =>
                    new DiffItemApiResponse
                    {
                        Offset = x.Offset,
                        Length = x.Length
                    })
            };

            return Request.CreateResponse(HttpStatusCode.OK, returnResult);
        }
    }
}
