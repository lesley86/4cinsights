﻿using System.Linq;
using System.Net.Http;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;
using _4cInsights.Core.Enums;
using _4cInsights.Core.Extensions;
using _4CInsights.Models;

namespace _4CInsights.Tests.Controllers
{
    [TestClass]
    public class DiffValueControllerTest : IntegrationTestFixture
    {      
        public DiffValueControllerTest()
        {
            _server = TestServer.Create(app =>
            {
                _container = new Container();
                var testConfiguration = new OwinTestConfiguration(_container);
                testConfiguration.Configuration(app);
            });
        }

        [TestMethod]
        public void Should_Return_Same_Length_For_Strings_That_Are_The_Same_Size()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientLeftRequest = new LeftValuePutRequest
                {
                    Data = "Testing put"
                };

                var putLeftResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientLeftRequest);

                var clientRightRequest = new RightValuePutRequest
                {
                    Data = "Testing put"
                };

                var putRightResponse = Put<RightValuePutRequest>("http://localhost/v1/diff/1/right", clientRightRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1");
                var result = response.Result.Content.ReadAsAsync<DiffApiResponse>();

                Assert.AreEqual(result.Result.DffResultType, EnumExtension.GetDescription(DiffResultTypeEnum.Equals));
            }           
        }

        [TestMethod]
        public void Should_Return_Not_Same_Size()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientLeftRequest = new LeftValuePutRequest
                {
                    Data = "Testing put"
                };

                var putLeftResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientLeftRequest);

                var clientRightRequest = new RightValuePutRequest
                {
                    Data = "Testing put2"
                };

                var putRightResponse = Put<RightValuePutRequest>("http://localhost/v1/diff/1/right", clientRightRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1");
                var result = response.Result.Content.ReadAsAsync<DiffApiResponse>();

                Assert.AreEqual(result.Result.DffResultType, EnumExtension.GetDescription(DiffResultTypeEnum.SizeDoNotMatch));
            }
        }

        [TestMethod]
        public void Should_Return_Offsets_For_Multiple_String_Differences()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientLeftRequest = new LeftValuePutRequest
                {
                    Data = "Tfsylbxfput"
                };

                var putLeftResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientLeftRequest);

                var clientRightRequest = new RightValuePutRequest
                {
                    Data = "Testing put"
                };

                var putRightResponse = Put<RightValuePutRequest>("http://localhost/v1/diff/1/right", clientRightRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1");
                var result = response.Result.Content.ReadAsAsync<DiffApiResponse>();

                Assert.AreEqual(result.Result.DffResultType, EnumExtension.GetDescription(DiffResultTypeEnum.ContentDoNotMatch));
                Assert.AreEqual(result.Result.Diffs.Count(), 2);
            }
        }

        [TestMethod]
        public void Should_Return_Offsets_For_One_String_Differenc_In_The_Begging_Of_The_String()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientLeftRequest = new LeftValuePutRequest
                {
                    Data = "fgxy"
                };

                var putLeftResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientLeftRequest);

                var clientRightRequest = new RightValuePutRequest
                {
                    Data = "Test"
                };

                var putRightResponse = Put<RightValuePutRequest>("http://localhost/v1/diff/1/right", clientRightRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1");
                var result = response.Result.Content.ReadAsAsync<DiffApiResponse>();

                var diffResult = result.Result.Diffs.FirstOrDefault();
                Assert.AreEqual(result.Result.DffResultType, EnumExtension.GetDescription(DiffResultTypeEnum.ContentDoNotMatch));
                Assert.AreEqual(diffResult.Length, 4);
               
            }
        }

        [TestMethod]
        public void Should_Return_Offsets_For_One_String_Differenc_At_The_End_Of_The_String()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientLeftRequest = new LeftValuePutRequest
                {
                    Data = "Tesy"
                };

                var putLeftResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientLeftRequest);

                var clientRightRequest = new RightValuePutRequest
                {
                    Data = "Test"
                };

                var putRightResponse = Put<RightValuePutRequest>("http://localhost/v1/diff/1/right", clientRightRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1");
                var result = response.Result.Content.ReadAsAsync<DiffApiResponse>();

                var diffResult = result.Result.Diffs.FirstOrDefault();
                Assert.AreEqual(result.Result.DffResultType, EnumExtension.GetDescription(DiffResultTypeEnum.ContentDoNotMatch));
                Assert.AreEqual(diffResult.Length, 1);

            }
        }
    }
}
