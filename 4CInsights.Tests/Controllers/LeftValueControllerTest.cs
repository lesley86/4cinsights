﻿using System.Net;
using System.Net.Http;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;
using _4CInsights.Models;

namespace _4CInsights.Tests.Controllers
{
    [TestClass]
    public class LeftValueControllerTest : IntegrationTestFixture
    {      
        public LeftValueControllerTest()
        {
            _server = TestServer.Create(app =>
            {
                _container = new Container();
                var testConfiguration = new OwinTestConfiguration(_container);
                testConfiguration.Configuration(app);
            });
        }

        [TestMethod]
        public void Save_And_Get_For_Should_Return_Results()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientRequest = new LeftValuePutRequest
                {
                    Data = "Testing put"
                };

                var putResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientRequest);

                var response = client.GetAsync("http://localhost/v1/diff/1/left");
                var result = response.Result.Content.ReadAsAsync<LeftValueApiResponse>();

                Assert.AreEqual(clientRequest.Data, result.Result.Data);
            }           
        }

        [TestMethod]
        public void Should_Return_Bad_Request_For_Null_Data()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var clientRequest = new LeftValuePutRequest
                {
                    Data = null
                };

                var putResponse = Put<LeftValuePutRequest>("http://localhost/v1/diff/1/left", clientRequest);
                Assert.AreEqual(putResponse.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        [TestMethod]
        public void Should_Return_Not_Found_If_The_Id_Does_Not_Exist()
        {
            using (var client = new HttpClient(_server.Handler))
            {
                var result = Get<LeftValueApiResponse>("http://localhost/v1/diff/25/left");
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
            }
        }

    }
}
