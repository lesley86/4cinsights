﻿using System.Web.Http;
using Owin;
using StructureMap;
using _4cInsights.DependencyResolution;

namespace _4CInsights.Tests
{
    public class OwinTestConfiguration
    {
        private readonly IContainer _container;

        public OwinTestConfiguration(IContainer container)
        {
            _container = container;
        }

        public void Configuration(IAppBuilder app)
        {
            // Build IoC Container
            ContainerBuilder.Initialize(_container);

            // Wire up IoC to MVC and Web API Dependency Resolver
            var configuration = new HttpConfiguration
            {
                DependencyResolver = new TestStructureMapResolver(_container)
            };

            WebApiConfig.Register(configuration);

            app.UseWebApi(configuration);
        }
    }
}
