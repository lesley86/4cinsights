﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Owin.Testing;
using Newtonsoft.Json;
using StructureMap;

namespace _4CInsights.Tests
{
    public class IntegrationTestFixture
    {
        protected IContainer _container;
        protected TestServer _server;

        protected HttpRequestMessage CreateRequestMessage(HttpMethod method, string requestUri, Dictionary<string, string> httpRequestHeaders = null)
        {
            var absoluteUri = new Uri(new Uri("http://localhost"), requestUri);
            var request = new HttpRequestMessage(method, absoluteUri);

            if (httpRequestHeaders != null)
            {
                foreach (var header in httpRequestHeaders)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return request;
        }

        public HttpResponseMessage Put<TBody>(string requestUri, TBody putBody)
        {
            var request = CreateRequestMessage(HttpMethod.Put, requestUri);

            request.Content = new StringContent(JsonConvert.SerializeObject(putBody), new UTF8Encoding(), "text/json");
            var task = _server.HttpClient.SendAsync(request);

            return task.IsFaulted ? null : task.Result;
        }

        public HttpResponseMessage Get<T>(string requestUri)
        {
            var request = CreateRequestMessage(HttpMethod.Get, requestUri);
            var task = _server.HttpClient.SendAsync(request);

            return task.IsFaulted ? null : task.Result;
        }
    }
}
