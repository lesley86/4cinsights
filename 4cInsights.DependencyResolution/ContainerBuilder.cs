﻿using _4cInsights.DependencyResolution.Registries.Service;
using StructureMap;
using _4cInsights.DependencyResolution.Registries.DataAccess;

namespace _4cInsights.DependencyResolution
{
    public static class ContainerBuilder
    {
        public static void Initialize(IContainer container)
        {
            container.Configure(x => {
                x.AddRegistry<ServiceRegistry>();
                x.AddRegistry<DataAccessRegistry>();
            });
        }
    }
}
