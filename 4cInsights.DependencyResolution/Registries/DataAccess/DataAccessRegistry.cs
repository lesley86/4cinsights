﻿using _4cInsights.Core.DataAccess.Interface;
using _4cInsights.DataAccess.Implementation;
using StructureMap;

namespace _4cInsights.DependencyResolution.Registries.DataAccess
{
    public class DataAccessRegistry : Registry
    {
        public DataAccessRegistry()
        {
            Scan(
             scanner =>
             {
                 scanner.AssemblyContainingType<ILeftValueDataAccess>();
                 scanner.AssemblyContainingType<LeftValueDataAccess>();
                 scanner.WithDefaultConventions();
             });
        }
    }
}
