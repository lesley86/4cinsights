﻿using _4cInsights.Core.Services.Interface;
using StructureMap;

namespace _4cInsights.DependencyResolution.Registries.Service
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            Scan(
              scanner =>
              {
                  scanner.AssemblyContainingType<ILeftValueService>();
                  scanner.WithDefaultConventions();
              });
        }
      
    }
}
