﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Http.Dependencies;
//using StructureMap;
//using System.Web;
//using _4CInsights;

//namespace _4cInsights.DependencyResolution
//{
//    public class StructureMapResolver : IDependencyResolver
//    {
//        private readonly IContainer _container;

//        public IDependencyScope BeginScope()
//        {
//            if (HttpContext.Current == null)
//            {
//                throw new InvalidOperationException("There is no current HttpContext");
//            }

//            var owinContext = HttpContext.Current.GetOwinContext();

//            var nestedContainer = owinContext.GetNestedContainer();

//            return new StructureMapResolver(nestedContainer);
//        }

//        public StructureMapResolver()
//        { }

//        public StructureMapResolver(IContainer container)
//        {
//            if (container == null) throw new ArgumentNullException("container");

//            _container = container;
//        }

//        public object GetService(Type serviceType)
//        {
//            if (serviceType == null)
//            {
//                return null;
//            }

//            if (serviceType.IsAbstract || serviceType.IsInterface)
//            {
//                return _container.TryGetInstance(serviceType);
//            }

//            return _container.GetInstance(serviceType);
//        }

//        public IEnumerable<object> GetServices(Type serviceType)
//        {
//            return _container.GetAllInstances(serviceType).Cast<object>();
//        }

//        public void Dispose()
//        {
//            // Container disposing is handled by StructureMapMiddleware.
//        }
//    }
//}
