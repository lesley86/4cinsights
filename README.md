## Synopsis

The api accepts string on two different endpoints. It saves these inputs into memory and when the third endpoint is called it will calculate if there is a difference in these strings. The overall logic is an outer loop that that navigates both strings. Once a difference is found a second loop determines  how long the difference occurs, then saving that as an offset.

* The architecture follows the [onion architecture ](http://jeffreypalermo.com/blog/the-onion-architecture-part-1/). 
* An Owin test server is used for integration tests.
* The dependency injection used is structure map.
* Moq was used for unit tests.

## Assumption
* Hosting would be done in IIS
* The Id's are Int
* No persistence of data is needed things would be created for the test then destroyed.
* No Authentication is required.

### These are some questions I asked that I used as assumptions
1. Due to the fact these words are said "actual diffs are not needed." I assume i do not need to decode the string and check the values that are in these strings. Is this correct?
2. Assuming number 1, does this mean that you want me to compare postion 1 in string 1 with postion 1 in string 2 then postion 2 in string 1 and postion 2 in string 2 and so on.
3. Assuming number 1, the offset in the string array is the first difference that occurs, the length is from that point how the long this occurrence lasts. Similar to the length of a sub string method. The offset 2 is the second difference and the length is how long that difference lasts?

## Decisions  
Used model for put requests, for flexibility allow for fields to be added fields later
Created a an internal and external model per layer to separate concerns

Empty strings are not a valid base 64 string



## Code Example

### If the strings are the same ###
PUT /v1/diff/1/left
{
  "data": "AAAAAA=="
}
response: 201 Created

PUT /v1/diff/1/right
{
  "data": "AAAAAA=="
}

response 201 Created

GET /v1/diff/1
response: 200 OK
{
  "diffResultType": "Equals"
}

### if the strings are the same size but have different values ###
PUT /v1/diff/1/right
{
  "data": "AQABAQ=="
}
response: 201 Created

GET /v1/diff/1
response: 200 OK
{
  "diffResultType": "ContentDoNotMatch",
  "diffs": [
    {
      "offset": 0,
      "length": 1
    },
    {
      "offset": 2,
      "length": 2
    }
  ]
}

### if the strings are different sizes ###
PUT /v1/diff/1/left
{
   "data": "AAA="
}
response: 201 Created

GET /v1/diff/1
response: 200 OK
{
  "diffResultType": "SizeDoNotMatch"
}