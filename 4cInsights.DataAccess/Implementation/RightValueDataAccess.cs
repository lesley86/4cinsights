﻿using _4cInsights.Core.DataAccess.Interface;
using _4cInsights.Core.DataAccess.Models.External;
using _4cInsights.Core.DataAccess.Models.Internal;
using System.Collections.Concurrent;

namespace _4cInsights.DataAccess.Implementation
{
    public class RightValueDataAccess : IRightValueDataAccess
    {
        // Create a new concurrent dictionary.
        static ConcurrentDictionary<int, RightValueDataAccessModel> _RightValues = new ConcurrentDictionary<int, RightValueDataAccessModel>();

        public RightValueDataAccessResponse Get(int Id)
        {
            RightValueDataAccessModel retrievedValue;
            if (_RightValues.TryGetValue(Id, out retrievedValue))
            {
                return new RightValueDataAccessResponse
                {
                    Id = retrievedValue.Id,
                    Data = retrievedValue.Data
                };
            }

            return null;
        }

        public RightValueDataAccessResponse UpdateRightValue(UpdateRightValueDataAccessRequest RightValueUpdateRequest)
        {
            RightValueDataAccessModel retrievedValue = new RightValueDataAccessModel
            {
                Id = RightValueUpdateRequest.Id,
                Data = RightValueUpdateRequest.Data
            };

            _RightValues.AddOrUpdate(RightValueUpdateRequest.Id, retrievedValue,
              (key, existingVal) =>
              {
                  existingVal.Data = RightValueUpdateRequest.Data;

                  return new RightValueDataAccessModel
                  {
                      Data = existingVal.Data,
                      Id = existingVal.Id
                  };
              });

            var entity = _RightValues[RightValueUpdateRequest.Id];
            return new RightValueDataAccessResponse
              {
                  Data = entity.Data,
                  Id = entity.Id
              };
        }

        public RightValueDataAccessResponse CreateRightValue(CreateRightValueDataAccessRequest RightValueCreateRequest)
        {
            //Convert to domain model
            var domainModel = new RightValueDataAccessModel
            {
                Id = RightValueCreateRequest.Id,
                Data = RightValueCreateRequest.Data
            };

            //Save The model

            //Return response
            return new RightValueDataAccessResponse
            {
                Id = domainModel.Id,
                Data = domainModel.Data
            };
        }
    }
}
