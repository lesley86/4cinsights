﻿using _4cInsights.Core.DataAccess.Interface;
using _4cInsights.Core.DataAccess.Models.External;
using _4cInsights.Core.DataAccess.Models.Internal;
using System.Collections.Concurrent;

namespace _4cInsights.DataAccess.Implementation
{
    public class LeftValueDataAccess : ILeftValueDataAccess
    {
        // Create a new concurrent dictionary.
        static ConcurrentDictionary<int, LeftValueDataAccessModel> _leftValues = new ConcurrentDictionary<int, LeftValueDataAccessModel>();

        public LeftValueDataAccessResponse Get(int Id)
        {
            LeftValueDataAccessModel entity = null;

            if (_leftValues.TryGetValue(Id, out entity))
            {
                return new LeftValueDataAccessResponse
                {
                    Id = entity.Id,
                    Data = entity.Data
                };
            }

            return null;
        }

        public LeftValueDataAccessResponse UpdateLeftValue(UpdateLeftValueDataAccessRequest leftValueUpdateRequest)
        {
            LeftValueDataAccessModel retrievedValue = new LeftValueDataAccessModel
            {
                Id = leftValueUpdateRequest.Id,
                Data = leftValueUpdateRequest.Data
            };

            _leftValues.AddOrUpdate(leftValueUpdateRequest.Id, retrievedValue,
              (key, existingVal) =>
              {
                  existingVal.Data = leftValueUpdateRequest.Data;

                  return new LeftValueDataAccessModel
                  {
                      Data = existingVal.Data,
                      Id = existingVal.Id
                  };
              });

            var entity = _leftValues[leftValueUpdateRequest.Id];
            return new LeftValueDataAccessResponse
              {
                  Data = entity.Data,
                  Id = entity.Id
              };
        }

        public LeftValueDataAccessResponse CreateLeftValue(CreateLeftValueDataAccessRequest leftValueCreateRequest)
        {
            //Convert to domain model
            var domainModel = new LeftValueDataAccessModel
            {
                Id = leftValueCreateRequest.Id,
                Data = leftValueCreateRequest.Data
            };

            //Save The model

            //Return response
            return new LeftValueDataAccessResponse
            {
                Id = domainModel.Id,
                Data = domainModel.Data
            };
        }
    }
}
