﻿using _4cInsights.Core.Service.Models.External;

namespace _4cInsights.Core.Services.Interface
{
    public interface ILeftValueService
    {
        int CreateOrUpdateLeftValue(UpdateLeftValueServiceRequest updateLeftValue);

        LeftValueServiceResponse GetLeftValue(int Id);
    }
}