﻿using _4cInsights.Core.Services.Models.External;

namespace _4cInsights.Core.Services.Interface
{
    public interface IDiffService
    {
        DiffServiceResponse GetDifferences(int Id);
    }
}
