﻿using _4cInsights.Core.Service.Models.External;

namespace _4cInsights.Core.Services.Interface
{
    public interface IRightValueService
    {
        int CreateOrUpdateRightValue(UpdateRightValueServiceRequest updateRightValue);

       RightValueServiceResponse GetRightValue(int Id);
    }
}
