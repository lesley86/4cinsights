﻿namespace _4cInsights.Core.Service.Models.External
{
    public class UpdateRightValueServiceRequest
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
