﻿namespace _4cInsights.Core.Services.Models.External
{
    public class DiffItem
    {
        public int Offset { get; set; }
        public int Length { get; set; }
    }
}
