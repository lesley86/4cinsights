﻿using System.Collections.Generic;

namespace _4cInsights.Core.Services.Models.External
{
    public class DiffServiceResponse
    {
        public string DffResultType { get; set; }

        public IEnumerable<DiffItem> Diffs { get; set; }
    }
}
