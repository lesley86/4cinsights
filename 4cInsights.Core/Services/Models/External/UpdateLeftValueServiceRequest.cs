﻿namespace _4cInsights.Core.Service.Models.External
{
    public class UpdateLeftValueServiceRequest
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
