﻿namespace _4cInsights.Core.Service.Models.External
{
    public class LeftValueServiceResponse
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
