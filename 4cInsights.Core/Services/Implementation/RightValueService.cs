﻿using _4cInsights.Core.Services.Interface;
using _4cInsights.Core.Service.Models.External;
using _4cInsights.Core.DataAccess.Interface;
using _4cInsights.Core.DataAccess.Models.External;
using System;
using _4cInsights.Core.Exceptions;

namespace _4cInsights.Core.Services.Implementation
{
    public class RightValueService :  IRightValueService
    {
        private readonly IRightValueDataAccess RightValueDataAccess;

        public RightValueService(IRightValueDataAccess RightValueDataAccess)
        {
            this.RightValueDataAccess = RightValueDataAccess;
        }

        public RightValueServiceResponse GetRightValue(int Id)
        {
            var entity = RightValueDataAccess.Get(Id);
            if (entity == null)
            {
                // Not Found Exception
                throw new NotFoundException(Guid.NewGuid().ToString());
            }

            return new RightValueServiceResponse
            {
                Id = entity.Id,
                Data = entity.Data
            };
        }

        public int CreateOrUpdateRightValue(UpdateRightValueServiceRequest updateRightValue)
        {
            var dataAccessResponse = new RightValueDataAccessResponse();

            if (string.IsNullOrWhiteSpace(updateRightValue.Data))
            {
                throw new ValidationException(Guid.NewGuid().ToString());
            }

            dataAccessResponse = RightValueDataAccess.UpdateRightValue(new UpdateRightValueDataAccessRequest
            {
                Id = updateRightValue.Id,
                Data = updateRightValue.Data
            });
       
            return dataAccessResponse.Id;
        }

    }
}
