﻿using _4cInsights.Core.Services.Interface;
using _4cInsights.Core.Service.Models.External;
using _4cInsights.Core.DataAccess.Interface;
using _4cInsights.Core.DataAccess.Models.External;
using System;
using _4cInsights.Core.Exceptions;

namespace _4cInsights.Core.Services.Implementation
{
    public class LeftValueService :  ILeftValueService
    {
        private readonly ILeftValueDataAccess leftValueDataAccess;

        public LeftValueService(ILeftValueDataAccess leftValueDataAccess)
        {
            this.leftValueDataAccess = leftValueDataAccess;
        }

        public LeftValueServiceResponse GetLeftValue(int Id)
        {
            var entity = leftValueDataAccess.Get(Id);
            if (entity == null)
            {
                // Not Found Exception
                throw new NotFoundException(Guid.NewGuid().ToString());
            }

            return new LeftValueServiceResponse
            {
                Id = entity.Id,
                Data = entity.Data
            };
        }

        public int CreateOrUpdateLeftValue(UpdateLeftValueServiceRequest updateLeftValue)
        {
            var dataAccessResponse = new LeftValueDataAccessResponse();

            if (string.IsNullOrWhiteSpace(updateLeftValue.Data))
            {
                throw new ValidationException(Guid.NewGuid().ToString());
            }

            dataAccessResponse = leftValueDataAccess.UpdateLeftValue(new UpdateLeftValueDataAccessRequest
            {
                Id = updateLeftValue.Id,
                Data = updateLeftValue.Data
            });
       
            return dataAccessResponse.Id;
        }

    }
}
