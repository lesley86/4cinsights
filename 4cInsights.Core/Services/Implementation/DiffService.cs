﻿using System.Collections.Generic;
using System.Linq;
using _4cInsights.Core.Extensions;
using _4cInsights.Core.Services.Interface;
using _4cInsights.Core.Services.Models.External;

namespace _4cInsights.Core.Services.Implementation
{
    public class DiffService : IDiffService
    {
        private readonly ILeftValueService _leftValueService;
        private readonly IRightValueService _rightValueService;

        public DiffService(
            ILeftValueService leftValueService,
            IRightValueService rightValueService)
        {
            _leftValueService = leftValueService;
            _rightValueService = rightValueService;
        }

        public DiffServiceResponse GetDifferences(int Id)
        {
            var leftValue = _leftValueService.GetLeftValue(Id);
            var rightValue = _rightValueService.GetRightValue(Id);

            DiffServiceResponse result = new DiffServiceResponse();

            // First check that the strings are equal.
            if (leftValue.Data.Length != rightValue.Data.Length)
            {
                result.DffResultType = EnumExtension.GetDescription(Enums.DiffResultTypeEnum.SizeDoNotMatch);
                return result;
            }

            var diffResult = CheckForStringDifferences(leftValue.Data, rightValue.Data);

            // if no differences were found then the strings are equal in value
            if (diffResult == null || !diffResult.Any())
            {
                result.DffResultType = EnumExtension.GetDescription(Enums.DiffResultTypeEnum.Equals);
                return result;
            }

            result.Diffs = diffResult;
            result.DffResultType = EnumExtension.GetDescription(Enums.DiffResultTypeEnum.ContentDoNotMatch);

            return result;
        }

        private List<DiffItem> CheckForStringDifferences(string leftValue, string rightValue)
        {
            var offSetIndex = 0;
            var differenceEndIndex = 0;
            var diffItems = new List<DiffItem>();

            // if the values are the same there won't be differences.
            if (leftValue == rightValue)
            {
                return new List<DiffItem>();
            }

            //outer loop to find offsets
            do
            {
                if (leftValue[offSetIndex] == rightValue[offSetIndex])
                {
                    offSetIndex = offSetIndex + 1;
                    continue;
                }

                // Inner loop: When offset is found, find length of the difference
                for (var j = offSetIndex + 1; j <= leftValue.Length; j++)
                {
                    differenceEndIndex = j;
                   
                    // If only one offset is found with no others we need to mark the last loop as the final index
                    if (j != leftValue.Length)
                    {
                        // if the next value after the offset is not equal continue until
                        // either the string ends or you find two equal chars in the strings.
                        if (leftValue[j] != rightValue[j] && j != leftValue.Length)
                        {
                            continue;
                        }
                    }                  

                    diffItems.Add(new DiffItem
                    {
                        Offset = offSetIndex,
                        Length = differenceEndIndex - offSetIndex
                    });

                    offSetIndex = differenceEndIndex + 1;
                    break;
                }
            } while (offSetIndex < leftValue.Length);

            return diffItems;
        }
    }
}
