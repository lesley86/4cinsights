﻿using System;

namespace _4cInsights.Core.Exceptions
{
    public class ValidationException : Exception
    {
        private readonly object _reference;

        public ValidationException(object reference)
        {
            _reference = reference;
        }

        public ValidationException(string message, object reference) : base(message)
        {
            _reference = reference;
        }

        public ValidationException(object reference, Exception innerException) : base(null, innerException)
        {
            _reference = reference;
        }

        public ValidationException(string message, object reference, Exception innerException)
            : base(message, innerException)
        {
            _reference = reference;
        }

        public object Reference
        {
            get { return _reference; }
        }
    }
}
