﻿namespace _4cInsights.Core.DataAccess.Models.Internal
{
    public class LeftValueDataAccessModel
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
