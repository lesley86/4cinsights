﻿namespace _4cInsights.Core.DataAccess.Models.External
{
    public class CreateRightValueDataAccessRequest
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
