﻿using _4cInsights.Core.DataAccess.Models.External;

namespace _4cInsights.Core.DataAccess.Interface
{
    public interface IRightValueDataAccess
    {
        RightValueDataAccessResponse Get(int Id);

        RightValueDataAccessResponse UpdateRightValue(UpdateRightValueDataAccessRequest RightValueUpdateRequest);

        RightValueDataAccessResponse CreateRightValue(CreateRightValueDataAccessRequest RightValueCreateRequest);
    }
}
