﻿using _4cInsights.Core.DataAccess.Models.External;

namespace _4cInsights.Core.DataAccess.Interface
{
    public interface ILeftValueDataAccess
    {
        LeftValueDataAccessResponse Get(int Id);

        LeftValueDataAccessResponse UpdateLeftValue(UpdateLeftValueDataAccessRequest leftValueUpdateRequest);

        LeftValueDataAccessResponse CreateLeftValue(CreateLeftValueDataAccessRequest leftValueCreateRequest);
    }
}
