﻿using System.ComponentModel;

namespace _4cInsights.Core.Enums
{
    public enum DiffResultTypeEnum
    {
        [Description("Unknown")]
        Unknown = 0,

        [Description("Equals")]
        Equals = 1,

        [Description("ContentDoNotMatch")]
        ContentDoNotMatch = 2, 

        [Description("SizeDoNotMatch")]
        SizeDoNotMatch = 3
    }
}
